package com.example.githubdemo.ui.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.githubdemo.R
import com.example.githubdemo.constant.INTENT_POST
import com.example.githubdemo.data.local.entity.GitHubEntity
import com.example.githubdemo.databinding.ActivityRepoDetailBinding

import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import java.lang.String

class GitHubDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityRepoDetailBinding
    var githubEntity: GitHubEntity? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRepoDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initializeExtras()
        initialiseView()
    }

    private fun initializeExtras() {
        githubEntity = intent.getParcelableExtra(INTENT_POST)
    }

    private fun initialiseView() {
        Picasso.get().load(githubEntity?.owner?.avatarUrl)
            .placeholder(R.drawable.ic_placeholder)
            .into(binding.itemProfileImg)
        binding.itemTitle.text = githubEntity?.fullName
        binding.itemStars.text = String.valueOf(githubEntity?.starsCount)
        binding.itemWatchers.text = String.valueOf(githubEntity?.watchers)
        binding.itemForks.text = String.valueOf(githubEntity?.forks)

        if (githubEntity?.language != null) {
            binding.itemImgLanguage.visibility = View.VISIBLE
            binding.itemLanguage.visibility = View.VISIBLE
            binding.itemLanguage.text = githubEntity?.language

        } else {
            binding.itemImgLanguage.visibility = View.INVISIBLE
            binding.itemLanguage.visibility = View.INVISIBLE
        }

    }




}