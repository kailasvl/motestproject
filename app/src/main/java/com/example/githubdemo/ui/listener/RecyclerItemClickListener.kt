package com.example.githubdemo.ui.listener

import android.content.Context
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class RecyclerItemClickListener(context: Context?, private val listner: OnRecyclerViewItemClickListener)
    : RecyclerView.OnItemTouchListener {

    private val mGestureDetector: GestureDetector = GestureDetector(context, object : SimpleOnGestureListener() {
        override fun onSingleTapUp(e: MotionEvent): Boolean {
            return true
        }
    })

    override fun onInterceptTouchEvent(view: RecyclerView, e: MotionEvent): Boolean {
        val childView: View? = view.findChildViewUnder(e.x, e.y)
        if (childView != null && listner != null && mGestureDetector.onTouchEvent(
                e
            )
        ) {
            listner.onItemClick(
                view,
                childView,
                view.getChildLayoutPosition(childView)
            )
        }
        return false
    }

    override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {}

    override fun onRequestDisallowInterceptTouchEvent(b: Boolean) {}

    interface OnRecyclerViewItemClickListener {
        fun onItemClick(
            parentView: View?,
            childView: View?,
            position: Int
        )
    }

}