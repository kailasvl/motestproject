package com.example.githubdemo.ui.main

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.work.WorkManager
import com.example.githubdemo.constant.QUERY_ORDER
import com.example.githubdemo.constant.QUERY_SORT
import com.example.githubdemo.data.local.database.LocalDataSource
import com.example.githubdemo.data.local.entity.GitHubEntity
import com.example.githubdemo.data.performGetOperation
import com.example.githubdemo.data.repository.ApiRepository
import com.example.githubdemo.worker.schedulesWorkerForMonitoring
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(var application: Application, private val apiRepository: ApiRepository, private val localDataSource : LocalDataSource) : ViewModel() {

    fun fetchGithubDirectory(pageNo : Long = 0)  = performGetOperation { apiRepository.fetchRepository(QUERY_SORT, QUERY_ORDER, pageNo) }

    fun fetchLocalGithubData(pageNo : Long = 0) : List<GitHubEntity>?  = localDataSource.fetchRepositories(pageNo)

    fun saveDataToGithubDatabase(items: ArrayList<GitHubEntity>?, pageNo: Long) =
        run {
            items?.forEach { it.page = pageNo }
            localDataSource.saveRepository(items)
            fetchLocalGithubData(pageNo)
        }


    fun startWorker() {
        schedulesWorkerForMonitoring( WorkManager.getInstance(application))
    }


}