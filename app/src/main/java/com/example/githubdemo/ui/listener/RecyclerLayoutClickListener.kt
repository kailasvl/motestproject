package com.example.githubdemo.ui.listener

import android.view.View
import com.example.githubdemo.data.local.entity.GitHubEntity

interface RecyclerLayoutClickListener {
    fun redirectToDetailScreen(
        imageView: View?,
        titleView: View?,
        revealView: View?,
        languageView: View?,
        githubEntity: GitHubEntity?
    )

    fun loadMore()
}