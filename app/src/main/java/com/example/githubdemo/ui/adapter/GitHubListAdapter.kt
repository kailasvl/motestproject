package com.example.githubdemo.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.example.githubdemo.R
import com.example.githubdemo.data.local.entity.GitHubEntity
import com.example.githubdemo.databinding.RepoListItemBinding
import com.example.githubdemo.ui.listener.RecyclerLayoutClickListener
import com.example.githubdemo.utils.getDate
import com.example.githubdemo.utils.getTime
import com.squareup.picasso.Picasso
import java.lang.String

public class GitHubListAdapter(var context: Context?, var clickListener: RecyclerLayoutClickListener) : RecyclerView.Adapter<GitHubListAdapter.VH_GITHUB>() {

    private var githubItems : ArrayList<GitHubEntity>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH_GITHUB {
        val binding = RepoListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        var viewHolder =  VH_GITHUB(binding)
        binding.cardView.setOnClickListener { viewHolder.onLayoutButtonClick() }
        return viewHolder
    }

    fun setItems(itemList : ArrayList<GitHubEntity>) {
        if(githubItems == null)
            githubItems = ArrayList()

        githubItems?.addAll(itemList)


    }

    override fun getItemCount(): Int {
        return githubItems?.size ?: 0
    }

    override fun onBindViewHolder(holder: VH_GITHUB, position: Int) {
        holder.bindTo(getItem(position))
        if (position == (itemCount - 1)) {
            clickListener.loadMore()
        }
    }

    private fun getItem(position: Int): GitHubEntity? {
        return githubItems?.get(position)
    }



    inner class VH_GITHUB(private val binding : RepoListItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindTo(githubEntity: GitHubEntity?) {
            Picasso.get().load(githubEntity?.owner?.avatarUrl)
                .placeholder(R.drawable.ic_placeholder)
                .into(binding.itemProfileImg)
            binding.itemTitle.text = githubEntity?.fullName
            binding.itemTime.text = String.format(
                context!!.getString(R.string.item_date),
                getDate(githubEntity?.createdAt),
                getTime(githubEntity?.createdAt)
            )
            binding.itemDesc.text = githubEntity?.description
            if (githubEntity?.language != null) {
                binding.itemImgLanguage.visibility = View.VISIBLE
                binding.itemLikes.visibility = View.VISIBLE
                binding.itemLikes.text = githubEntity.language
            } else {
                binding.itemLikes.visibility = View.GONE
                binding.itemImgLanguage.visibility = View.GONE
            }
        }


        fun onLayoutButtonClick() {
            clickListener.redirectToDetailScreen(
                binding.itemProfileImg,
                binding.itemTitle,
                binding.itemImgLanguage,
                binding.itemLikes,
                getItem(layoutPosition)
            )
        }
    }
}
