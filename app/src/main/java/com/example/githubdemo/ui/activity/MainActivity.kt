package com.example.githubdemo.ui.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubdemo.R
import com.example.githubdemo.constant.INTENT_POST
import com.example.githubdemo.data.local.entity.GitHubEntity
import com.example.githubdemo.data.model.Resource
import com.example.githubdemo.databinding.ActivityMainBinding
import com.example.githubdemo.ui.adapter.FilterListAdapter
import com.example.githubdemo.ui.adapter.GitHubListAdapter
import com.example.githubdemo.ui.listener.RecyclerItemClickListener
import com.example.githubdemo.ui.listener.RecyclerLayoutClickListener
import com.example.githubdemo.ui.main.MainViewModel
import com.example.githubdemo.utils.isNetworkOnline
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity(), RecyclerLayoutClickListener {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()
    private var githubListAdapter: GitHubListAdapter? = null
    private var filterListAdapter: FilterListAdapter? = null
    private var pageNo: Long = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initialiseView()
        fetchGithubData()
        viewModel.startWorker()
    }

    private fun initialiseView() {
        setSupportActionBar(binding.mainToolbar.toolbar)



        binding.recyclerView.layoutManager = LinearLayoutManager(applicationContext)
        githubListAdapter = GitHubListAdapter(applicationContext, this)
        binding.recyclerView.adapter = githubListAdapter

    }

    private fun isOnline(): Boolean {
        return isNetworkOnline(this)
    }

    private fun fetchGithubData() {
        if (isOnline()) {
            loadDataFromNetwork()
        } else {
            loadDataFromDatabase()
        }
    }

    private fun loadDataFromNetwork() {
        viewModel.fetchGithubDirectory(pageNo).observe(this) {
            when (it.status) {
                Resource.Status.LOADING -> {
                    displayLoader()
                }
                Resource.Status.SUCCESS -> {
                    hideLoader()
                    val response = it.data
                    if (response?.items != null && response.items!!.size > 0) {
                        saveDataToLocal(response.items!!)
                        displayDataView(response.items!!)
                    } else
                        displayEmptyView()
                }
                Resource.Status.FAILURE -> {
                    hideLoader()
                    loadDataFromDatabase()
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun displayLoader() {
        binding.viewLoader.rootView.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        binding.viewLoader.rootView.visibility = View.GONE
    }

    private fun displayEmptyView() {
        if (githubListAdapter?.itemCount ?: 0 <= 0) {
            hideLoader()
            binding.recyclerView.visibility = View.GONE
            binding.viewEmpty.emptyContainer.visibility = View.VISIBLE
        }
    }

    private fun saveDataToLocal(repositories: ArrayList<GitHubEntity>) {
        viewModel.saveDataToGithubDatabase(repositories, pageNo)
    }

    private fun loadDataFromDatabase() {
        val items: ArrayList<GitHubEntity>? =
            viewModel.fetchLocalGithubData(pageNo) as ArrayList<GitHubEntity>?
        if (items?.isEmpty() == true)
            displayEmptyView()
        else
            displayDataView(items!!)
    }

    private fun animateView(repositories: ArrayList<GitHubEntity>) {
        hideLoader()
//        slideView(binding.filterLayout, binding.filterList, filterListAdapter)
        displayDataView(repositories)
        binding.recyclerView.scheduleLayoutAnimation()
    }

    private fun displayDataView(repositories: ArrayList<GitHubEntity>) {
        binding.viewEmpty.emptyContainer.visibility = View.GONE
        binding.recyclerView.visibility = View.VISIBLE
        githubListAdapter?.setItems(repositories)
        githubListAdapter?.notifyDataSetChanged()
    }

    override fun redirectToDetailScreen(
        imageView: View?,
        titleView: View?,
        revealView: View?,
        languageView: View?,
        githubEntity: GitHubEntity?
    ) {
        val intent = Intent(this, GitHubDetailsActivity::class.java)
        intent.putExtra(INTENT_POST, githubEntity)
        startActivity(intent)
    }

    override fun loadMore() {
        Log.d("Kailas", "Page no = $pageNo")
        pageNo++
        fetchGithubData()
    }
}