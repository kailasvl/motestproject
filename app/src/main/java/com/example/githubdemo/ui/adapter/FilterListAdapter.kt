package com.example.githubdemo.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.githubdemo.databinding.FilterItemBinding
import com.example.githubdemo.ui.adapter.FilterListAdapter.CustomViewHolder

public class FilterListAdapter(private val items: ArrayList<String>) : RecyclerView.Adapter<CustomViewHolder?>() {
    private var lastSelectedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding: FilterItemBinding =
            FilterItemBinding.inflate(layoutInflater, parent, false)
        return CustomViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val item = getItem(position)
        holder.binding.btnRadio.text = item
        holder.binding.btnRadio.isChecked = lastSelectedPosition == position
    }

    override fun getItemCount(): Int {
       return items.size
    }

    public fun getItem(position: Int): String {
        return items[position]
    }

    fun updateSelection(currentPosition: Int) {
        val previousPosition = lastSelectedPosition
        lastSelectedPosition = currentPosition
        notifyItemChanged(previousPosition)
        notifyItemChanged(lastSelectedPosition)
    }

     inner class CustomViewHolder(var binding: FilterItemBinding) : RecyclerView.ViewHolder(binding.root) {

         init {
             binding.btnRadio.isEnabled = false
        }
    }

}