package com.example.githubdemo.constant

const val QUERY_API = "android"

const val PAGE_MAX_SIZE = "80"

const val QUERY_SORT = "stars"

const val QUERY_ORDER = "desc"

const val INTENT_POST = "intent_post"

const val MONITOR_INTERVAL: Long = 10//15 * 60