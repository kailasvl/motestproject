package com.example.githubdemo.di

import android.app.Application
import androidx.annotation.NonNull
import androidx.room.Room
import com.example.githubdemo.data.local.dao.GitHubDao
import com.example.githubdemo.data.local.database.GitHubDatabase
import com.example.githubdemo.data.local.database.LocalDataSource
import com.example.githubdemo.data.local.entity.GitHubEntity
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DBModule  {

    @Provides
    @Singleton
    fun provideDatabase(@NonNull application: Application): LocalDataSource {
        return LocalDataSource(application)
    }

    @Provides
    @Singleton
    fun provideGithubDao(@NonNull appDatabase: GitHubDatabase): GitHubDao? {
        return appDatabase.githubDao()
    }
}