package com.example.githubdemo

import android.app.Application
import androidx.work.Configuration
import com.example.githubdemo.data.local.database.LocalDataSource
import com.example.githubdemo.data.repository.ApiRepository
import com.example.githubdemo.worker.MyWorkerFactory
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class GitHubApplication : Application(), Configuration.Provider {

    @Inject
    lateinit var apiRepository : ApiRepository

    @Inject
    lateinit var localDataSource: LocalDataSource


    override fun onCreate() {
        super.onCreate()
    }

    override fun getWorkManagerConfiguration(): Configuration =
        Configuration.Builder()
            .setMinimumLoggingLevel(android.util.Log.DEBUG)
            .setWorkerFactory(MyWorkerFactory(apiRepository, localDataSource))
            .build()
}