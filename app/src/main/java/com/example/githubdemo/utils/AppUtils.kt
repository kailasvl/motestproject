package com.example.githubdemo.utils

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.graphics.drawable.DrawableContainer
import android.graphics.drawable.GradientDrawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.NetworkInfo
import android.os.Build
import androidx.core.content.ContextCompat
import com.example.githubdemo.R
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.min

const val DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"

var LANGUAGE_COLOR_MAP: MutableMap<String, Int> =
    Collections.unmodifiableMap<String, Int>(
        object : HashMap<String?, Int?>() {
            init {
                put("Java", R.color.colorPrimary)
                put("Kotlin", R.color.color_orange)
                put("Dart", R.color.color_blue)
                put("JavaScript", R.color.color_yellow)
                put("CSS", R.color.color_yellow)
            }
        })

fun getDate(dateString: String?): String? {
    return try {
        val format1 = SimpleDateFormat(DATE_TIME_FORMAT)
        val date = format1.parse(dateString)
        val sdf: DateFormat = SimpleDateFormat("MMM d, yyyy")
        sdf.format(date)
    } catch (ex: Exception) {
        ex.printStackTrace()
        "xx"
    }
}


fun getTime(dateString: String?): String? {
    return try {
        val format1 = SimpleDateFormat(DATE_TIME_FORMAT)
        val date = format1.parse(dateString)
        val sdf: DateFormat = SimpleDateFormat("h:mm a")
        sdf.format(date)
    } catch (ex: java.lang.Exception) {
        ex.printStackTrace()
        "xx"
    }
}






fun isNetworkOnline(context: Context?): Boolean {
    var status = false
    if (context != null) {
        val connManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connManager != null) {
            var mWifi: NetworkInfo? = null
            mWifi = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                connManager.getNetworkInfo(NetworkCapabilities.TRANSPORT_WIFI)
            } else {
                connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            }
            if (mWifi != null && mWifi.isConnected) {
                return true
            }
            try {
                var netInfo = connManager.getNetworkInfo(0)
                if (netInfo != null && netInfo.state == NetworkInfo.State.CONNECTED) {
                    status = true
                } else {
                    netInfo = connManager.getNetworkInfo(1)
                    if (netInfo != null && netInfo.state == NetworkInfo.State.CONNECTED) status =
                        true
                }
            } catch (e: java.lang.Exception) {
                return false
            }
        }
    }
    return status
}