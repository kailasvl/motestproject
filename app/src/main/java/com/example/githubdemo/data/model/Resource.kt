package com.example.githubdemo.data.model

data class Resource<out T>(val status: Status, val data: T?, val message: String?, val pageNo: Long = 0) {

    enum class Status {
        SUCCESS,
        FAILURE,
        LOADING,
        CACHED
    }

    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(message: String, data: T? = null): Resource<T> {
            return Resource(Status.FAILURE, data, message)
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }

    }
}
interface networkListener<T> {

    enum class Status {
        SUCCESS,
        FAILURE,
        LOADING,
        CACHED
    }

    fun onSuccess(data : T)

    fun onError(message: String?)

}