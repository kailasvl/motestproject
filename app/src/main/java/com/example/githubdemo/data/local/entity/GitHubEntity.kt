package com.example.githubdemo.data.local.entity

import android.os.Parcelable
import androidx.annotation.NonNull
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.githubdemo.data.local.converter.TimestampConverter
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
class GitHubEntity(
    @NonNull @PrimaryKey var id: Long? = null,
    @TypeConverters(TimestampConverter::class) @SerializedName("created_at") var createdAt: String? = null,
    @SerializedName("full_name") var fullName: String? = null,
    @SerializedName("html_url") var htmlUrl: String? = null,
    @SerializedName("contributors_url") var contributorsUrl: String? = null,
    @SerializedName("stargazers_count") var starsCount: Long? = null,
    @Embedded @SerializedName("owner") var owner: Owner? = null,
    var page: Long? = null,
    var totalPages: Long? = null,
    var name: String? = null,
    var description: String? = null,
    var watchers: Long? = null,
    var forks: Long? = null,
    var language: String? = null
) : Parcelable
