package com.example.githubdemo.data.repository

import com.example.githubdemo.data.getResult
import com.example.githubdemo.data.remote.ApiService
import javax.inject.Inject

class ApiRepository @Inject constructor(private val apiService: ApiService) {

    suspend fun fetchRepository(sort: String, order: String, pageNo: Long) =
        getResult {  apiService.fetchRepositories(sort, order, pageNo) }
}
