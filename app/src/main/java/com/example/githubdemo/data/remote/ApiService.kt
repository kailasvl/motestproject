package com.example.githubdemo.data.remote

import androidx.lifecycle.LiveData
import com.example.githubdemo.data.model.GitHubResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("search/repositories")
    suspend fun fetchRepositories(
        @Query("sort") sort: String,
        @Query("order") order: String,
        @Query("page") page: Long
    ): Response<GitHubResponse>
}