package com.example.githubdemo.data.local.entity

import android.os.Parcelable
import androidx.room.Entity
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
class Owner(
    var login: String? = null,
    @SerializedName("avatar_url") var avatarUrl: String? = null
) : Parcelable