package com.example.githubdemo.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.githubdemo.data.local.converter.TimestampConverter
import com.example.githubdemo.data.local.dao.GitHubDao
import com.example.githubdemo.data.local.entity.GitHubEntity

@Database(entities = [GitHubEntity::class], version = 1, exportSchema = false)
@TypeConverters(TimestampConverter::class)
abstract class GitHubDatabase : RoomDatabase() {

    abstract fun githubDao(): GitHubDao?

}
