package com.example.githubdemo.data.model

import com.example.githubdemo.data.local.entity.GitHubEntity
import com.google.gson.annotations.SerializedName

data class GitHubResponse (@SerializedName("total_count") var totalCount : Long? = 0,
                           @SerializedName("items")  var items : ArrayList<GitHubEntity>? = null)
