package com.example.githubdemo.data.remote

import com.example.githubdemo.constant.PAGE_MAX_SIZE
import com.example.githubdemo.constant.QUERY_API
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject
import kotlin.jvm.Throws


class RequestInterceptor @Inject constructor(): Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        val originalUrl = originalRequest.url
        val url = originalUrl.newBuilder()
            .addQueryParameter("q", QUERY_API)
            .addQueryParameter("per_page", PAGE_MAX_SIZE)
            .build()
        val requestBuilder = originalRequest.newBuilder().url(url)
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}