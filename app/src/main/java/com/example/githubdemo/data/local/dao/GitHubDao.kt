package com.example.githubdemo.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.githubdemo.data.local.entity.GitHubEntity

@Dao
public interface GitHubDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRepositories(githubEntities: List<GitHubEntity>?): LongArray?

    @Query("SELECT * FROM `GithubEntity` where page = :page")
    fun getRepositoriesByPage(page: Long?): List<GitHubEntity>?
}