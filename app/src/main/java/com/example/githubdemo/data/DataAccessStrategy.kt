package com.example.githubdemo.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import com.example.githubdemo.data.model.GitHubResponse
import com.example.githubdemo.data.model.Resource
import kotlinx.coroutines.Dispatchers
import retrofit2.Response

fun <T> performGetOperation(networkCall: suspend () -> Resource<T>): LiveData<Resource<T>> =
    liveData(Dispatchers.IO) {
        emit(Resource.loading())
        val responseStatus = networkCall.invoke()
        if (responseStatus.status == Resource.Status.SUCCESS) {
            emit(Resource.success(responseStatus.data!!))
        } else if (responseStatus.status == Resource.Status.FAILURE) {
            emit(Resource.error(responseStatus.message!!))
        }
    }

suspend fun <T> getResult(call: suspend () -> Response<T>): Resource<T> {
    try {
        val response = call()
        if (response.isSuccessful) {
            val body = response.body()
            if (body != null) return Resource.success(body)
        }
        return error(" ${response.code()} ${response.message()}")
    } catch (e: Exception) {
        return error(e.message ?: e.toString())
    }
}

fun <T> error(message: String): Resource<T> {
    if (message.contains("ETIMEDOUT", false)) {
        return Resource.error("Connection timed out")
    }
    return Resource.error("Something went wrong, Please try again later")
}