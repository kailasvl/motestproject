package com.example.githubdemo.data.local.database

import android.app.Application
import android.util.Log
import androidx.annotation.NonNull
import androidx.room.Room
import com.example.githubdemo.data.local.entity.GitHubEntity
import javax.inject.Inject

class LocalDataSource @Inject constructor(@NonNull var application: Application) {

    private var githubDatabase : GitHubDatabase? = null

    init {
        githubDatabase =   Room.databaseBuilder(application!!, GitHubDatabase::class.java, "Github.db")
            .allowMainThreadQueries().build()
    }

    fun fetchRepositories(
        page: Long
    ): List<GitHubEntity>? {
        var items = githubDatabase?.githubDao()?.getRepositoriesByPage(page)
        Log.d("data", "page no = "+ page +" , items size" + items?.size)
        return items
    }

    fun saveRepository(items : List<GitHubEntity>?) {
        githubDatabase?.githubDao()?.insertRepositories(items)
    }
}

