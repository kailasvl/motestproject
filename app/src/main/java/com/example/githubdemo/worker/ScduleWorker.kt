package com.example.githubdemo.worker

import android.os.Build
import androidx.work.*
import java.util.concurrent.TimeUnit

const val MONITOR_INTERVAL: Long = 15*60// min 15 required testing purpose you can change to 10 seconds

fun getConstraints(): Constraints {
    val builder = Constraints.Builder()
    builder.setRequiresCharging(false)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        builder.setRequiresDeviceIdle(false)
    }
    return builder.build()
}

fun schedulesWorkerForMonitoring(workManager: WorkManager) {
    val data = Data.Builder()
        .build()
    val periodicWorkRequest = PeriodicWorkRequest.Builder(
        SyncDataWorker::class.java,
        MONITOR_INTERVAL,
        TimeUnit.SECONDS
    )
        .setInputData(data)
        .setConstraints(getConstraints())
        .build()
    workManager
        .enqueueUniquePeriodicWork(
            "Monitoring",
            ExistingPeriodicWorkPolicy.REPLACE,
            periodicWorkRequest
        )
}
