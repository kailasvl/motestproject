package com.example.githubdemo.worker

import android.content.Context
import androidx.work.ListenableWorker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import com.example.githubdemo.data.local.database.LocalDataSource
import com.example.githubdemo.data.remote.ApiService
import com.example.githubdemo.data.repository.ApiRepository

class MyWorkerFactory(private val apiService: ApiRepository, private val localDataSource: LocalDataSource) : WorkerFactory() {

    override fun createWorker(
        appContext: Context,
        workerClassName: String,
        workerParameters: WorkerParameters
    ): ListenableWorker? {
        return SyncDataWorker(appContext, workerParameters, apiService, localDataSource)
    }
}