package com.example.githubdemo.worker

import android.app.Application
import android.content.Context
import android.util.Log

import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.githubdemo.constant.QUERY_ORDER
import com.example.githubdemo.constant.QUERY_SORT
import com.example.githubdemo.data.local.database.LocalDataSource
import com.example.githubdemo.data.repository.ApiRepository
import com.google.gson.Gson


class SyncDataWorker(var context: Context, params: WorkerParameters,var apiService: ApiRepository, var localDataSource : LocalDataSource) : CoroutineWorker(context, params){

    override suspend fun doWork(): Result {
        return try {
            val data = apiService.fetchRepository(QUERY_SORT, QUERY_ORDER, 0)
            if(data?.data?.items != null && data.data.items!!.size > 0) {
                localDataSource.saveRepository(data.data.items!!)
            }
            Log.d("Woker", " GSOnnnnn = " + Gson().toJson(data))

            val item = localDataSource.fetchRepositories(0)
            Log.d("Woker", " GSOn = " + Gson().toJson(item))
            Result.success()
        } catch (throwable: Throwable) {
            Result.failure()
        }
    }
}