package com.example.githubdemo.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.example.githubdemo.worker.SyncDataWorker
import com.example.githubdemo.worker.schedulesWorkerForMonitoring

class BootReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        intent ?: return
        context ?: return

        val action = intent.action ?: return
        if (action == Intent.ACTION_BOOT_COMPLETED || action == Intent.ACTION_LOCKED_BOOT_COMPLETED) {
//            WorkManager.getInstance(context).enqueue(OneTimeWorkRequest.from(SyncDataWorker::class.java))
            schedulesWorkerForMonitoring( WorkManager.getInstance(context))
        }
    }
}